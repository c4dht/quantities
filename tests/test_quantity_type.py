"""
Tests for the QuantityType class
"""

import pytest

from src.qnit import (
    errors,
    quantity_types,
    units_types,
    units_collections,
)


# noinspection PyUnusedLocal
def test_quantity_types():
    """
    Test the cases if the during runtime specified units for the given parameter
    do not comply with the declared units type of the parameter
    """
    # Quantity types without default display units must be possible
    q1: quantity_types.QuantityType[units_types.Power] = (
        quantity_types.QuantityType(
            units_type=units_types.Power,
            internal_units=units_collections.Power.kW,
            available_units=units_collections.Power(),
        )
    )

    # Non-compatible internal units should raise QuantityUnitsError
    with pytest.raises(errors.QuantityUnitsError):
        q2: quantity_types.QuantityType[units_types.Power] = (
            quantity_types.QuantityType(
                units_type=units_types.Power,
                internal_units=units_collections.Energy.kWh,  # type: ignore[arg-type]
                default_display_units=units_collections.Energy.kWh,  # type: ignore[arg-type]
                available_units=units_collections.Power(),
            )
        )

    # Non-compatible available units should raise QuantityUnitsError
    with pytest.raises(errors.QuantityUnitsError):
        q3: quantity_types.QuantityType[units_types.Power] = (
            quantity_types.QuantityType(
                units_type=units_types.Power,
                internal_units=units_collections.Power.W,
                default_display_units=units_collections.Power.kW,
                available_units=units_collections.Energy(),
            )
        )

    # Non-compatible default display units should raise QuantityUnitsError
    with pytest.raises(errors.QuantityUnitsError):
        # noinspection PyTypeChecker
        q4: quantity_types.QuantityType[units_types.Power] = (
            quantity_types.QuantityType(
                units_type=units_types.Power,
                internal_units=units_collections.Power.kW,
                default_display_units=units_collections.Energy.kWh,  # type: ignore[arg-type]
                available_units=units_collections.Power(),
            )
        )
