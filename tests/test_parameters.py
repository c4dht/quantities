"""
Tests for the Parameter class
"""

from collections.abc import Sequence
from typing import Tuple, Union

import pint.testing
import pytest
import numpy as np
import numpy.typing as npt

from src.qnit import (
    Quantity,
    Parameter,
    errors,
    PintQuantity,
    quantity_types,
    units_types,
    units_collections,
)
from tests import FOO, BAR, SCALAR_MAGNITUDE, ARRAY_MAGNITUDE
from tests.conftest import (
    thermal_power_parameter,
    thermal_power_parameter_set,
    thermal_power_parameter_array_set,
    fuel_type_parameter,
    fuel_type_parameter_set,
    fuel_type_parameter_array_set,
)


THERMAL_POWER_PARAM_SET_FIXTURES = (
    thermal_power_parameter_set.__name__,
    thermal_power_parameter_array_set.__name__,
)

FUEL_TYPE_PARAM_SET_FIXTURES = (
    fuel_type_parameter_set.__name__,
    fuel_type_parameter_array_set.__name__,
)


@pytest.mark.parametrize(
    "thermal_power_param_set_func", THERMAL_POWER_PARAM_SET_FIXTURES
)
def test_units_aware_parameters(
    thermal_power_param_set_func: str, request: pytest.FixtureRequest
):
    """
    Test units-aware parameter functionality
    :param thermal_power_param_set_func: Name of the thermal power parameter fixture function
    """
    fixture_return: Tuple[
        Parameter[float, units_types.Power], Union[float, npt.NDArray]
    ] = request.getfixturevalue(thermal_power_param_set_func)
    parameter, magnitude = fixture_return

    assert parameter.pint_quantity is not None
    assert parameter.quantity is not None
    assert parameter.value is not None

    # Parameter is units-aware
    assert parameter.is_units_aware is True

    # Units-aware parameter is of QuantityType
    pint.testing.assert_equal(
        parameter.pint_quantity, PintQuantity(magnitude, "kW")
    )

    assert parameter.quantity == Quantity(
        quantity_type=quantity_types.Power,
        magnitude=magnitude,
        units=units_collections.Power.kW,
    )

    # Setting incompatible units should raise a QuantityUnitsError for units-aware parameter
    with pytest.raises(errors.QuantityUnitsError):
        # noinspection PyTypeChecker
        parameter.set_magnitude_units(
            magnitude=magnitude, units=units_collections.Energy.kWh  # type: ignore[arg-type]
        )

    # Accessing value should return the quantity
    assert parameter.value == Quantity(
        quantity_type=quantity_types.Power,
        magnitude=magnitude,
        units=units_collections.Power.kW,
    )

    # Accessing magnitudes should work as expected
    assert np.equal(
        parameter.magnitude(units=units_collections.Power.kW),
        magnitude,
    ).all()
    assert np.equal(
        parameter.internal_magnitude,
        np.multiply(magnitude, 1e3),
    ).all()
    assert np.equal(parameter.display_magnitude, magnitude).all()


@pytest.mark.parametrize(
    "fuel_type_param_set_func", FUEL_TYPE_PARAM_SET_FIXTURES
)
def test_units_naive_parameters(
    fuel_type_param_set_func: str, request: pytest.FixtureRequest
):
    """
    Test units-naive parameter functionality
    :param fuel_type_param_set_func: Name of the fuel type parameter fixture function
    """
    fixture_return: Tuple[
        Parameter[str, units_types.NoUnits], Union[str, npt.NDArray[np.str_]]
    ] = request.getfixturevalue(fuel_type_param_set_func)
    parameter, value = fixture_return

    # Accessing pint_quantity or quantity should raise a ParameterValueError for units-naive parameter
    with pytest.raises(errors.ParameterValueError):
        parameter.pint_quantity = PintQuantity(value, "kW")
    with pytest.raises(errors.ParameterValueError):
        parameter.quantity = Quantity(
            quantity_type=quantity_types.Power,  # type: ignore[arg-type]
            description="Test quantity for thermal power",
        )

    # Setting magnitude and units for units-naive parameter should raise a ParameterValueError
    with pytest.raises(errors.ParameterValueError):
        # noinspection PyTypeChecker
        parameter.set_magnitude_units(
            magnitude=value, units=units_collections.Power.kW  # type: ignore[arg-type]
        )

    # Accessing magnitude should raise a ParameterValueError for units-naive parameter
    with pytest.raises(errors.ParameterValueError):
        # noinspection PyTypeChecker
        assert np.equal(
            parameter.magnitude(units=units_collections.Power.kW),  # type: ignore[arg-type]
            value,
        ).all()


@pytest.mark.parametrize(
    "magnitude",
    (SCALAR_MAGNITUDE, ARRAY_MAGNITUDE),
)
def test_set_aware(
    thermal_power_parameter: Parameter[float, units_types.Power],
    magnitude: Union[int, npt.NDArray[np.number]],
):
    """
    Test set methods for units-aware parameters
    :param thermal_power_parameter: [Fixture] An unset thermal power parameter
    """
    # There are five different options to set a value for units-aware parameter
    # Option 1 by set_magnitude_units() method
    thermal_power_parameter.set_magnitude_units(
        magnitude=magnitude, units=units_collections.Power.kW
    )

    pint.testing.assert_equal(
        thermal_power_parameter.pint_quantity, PintQuantity(magnitude, "kW")
    )
    # Option 2 by set_value() method
    thermal_power_parameter.set_value(
        value=Quantity(
            quantity_type=quantity_types.Power,
            magnitude=magnitude,
            units=units_collections.Power.kW,
        )
    )
    pint.testing.assert_equal(
        thermal_power_parameter.pint_quantity, PintQuantity(magnitude, "kW")
    )
    # Option 3 by pint_quantity.setter
    thermal_power_parameter.pint_quantity = PintQuantity(magnitude, "kW")

    assert thermal_power_parameter.quantity == Quantity(
        quantity_type=quantity_types.Power,
        magnitude=magnitude,
        units=units_collections.Power.kW,
    )

    # Option 4 by quantity.setter
    thermal_power_parameter.quantity = Quantity(
        quantity_type=quantity_types.Power,
        magnitude=magnitude,
        units=units_collections.Power.kW,
    )

    assert thermal_power_parameter.value == Quantity(
        quantity_type=quantity_types.Power,
        magnitude=magnitude,
        units=units_collections.Power.kW,
    )

    # Option 5 by value.setter
    thermal_power_parameter.value = Quantity(
        quantity_type=quantity_types.Power,
        magnitude=magnitude,
        units=units_collections.Power.kW,
    )

    pint.testing.assert_equal(
        thermal_power_parameter.pint_quantity, PintQuantity(magnitude, "kW")
    )


@pytest.mark.parametrize(
    "value",
    (FOO, [FOO, BAR]),
)
def test_set_naive(
    fuel_type_parameter: Parameter[str, units_types.NoUnits],
    value: Union[str, Sequence[str]],
):
    """
    Test set methods for units-naive parameters
    :param fuel_type_parameter: [Fixture] An unset fuel type parameter
    """
    value_comment = "Reference for value"
    # There are two options to set a value for units-naive parameter
    # Option 1 by set_value() method
    fuel_type_parameter.set_value(value=value, value_comment=value_comment)
    assert not isinstance(fuel_type_parameter.value, Quantity)
    assert fuel_type_parameter.value is not None
    assert np.equal(fuel_type_parameter.value, value).all()
    assert fuel_type_parameter.valueComment == value_comment

    # Option 2 by value.setter
    # TODO Paul: The following type ignore should be removed when mypy issue 3004 is resolved
    # https://github.com/python/mypy/issues/3004
    fuel_type_parameter.value = value  # type: ignore[assignment]
    assert not isinstance(fuel_type_parameter.value, Quantity)
    assert fuel_type_parameter.value is not None
    assert np.equal(fuel_type_parameter.value, value).all()
    # The value comment should still be there
    assert fuel_type_parameter.valueComment == value_comment

    # Setting value by set_magnitude_units() method should raise a ParameterValueError for units-naive parameter
    with pytest.raises(errors.ParameterValueError):
        # noinspection PyTypeChecker
        fuel_type_parameter.set_magnitude_units(
            magnitude=SCALAR_MAGNITUDE,  # type: ignore[arg-type]
            units=units_collections.Dimensionless.dimensionless,  # type: ignore[arg-type]
        )

    # Setting value with incompatible data types should raise a ParameterValueError
    class NoStringCasting(np.str_):
        """
        A class which numpy recognizes as scalar but is not compatible with str
        """

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)

        def __str__(self):
            raise ValueError

    with pytest.raises(errors.ParameterValueError):
        fuel_type_parameter.value = NoStringCasting()

    # Setting value by pint_quantity.setter should raise a ParameterValueError for units-naive parameter
    with pytest.raises(errors.ParameterValueError):
        fuel_type_parameter.pint_quantity = PintQuantity(value, "kW")

    # Setting value by quantity.setter should raise a ParameterValueError for units-naive parameter
    with pytest.raises(errors.ParameterValueError):
        fuel_type_parameter.quantity = Quantity(
            quantity_type=quantity_types.Power,  # type: ignore[arg-type]
            description="Test quantity for thermal power",
        )


def test_parameter_initializing():
    # Setting a non-quantity value for a units-aware parameter should raise an error
    with pytest.raises(errors.ParameterValueError):
        _power_no_quantity_str: Parameter[float, units_types.Power] = Parameter(
            data_type=float,
            quantity_type=quantity_types.Power,
            description="Units-aware parameter for thermal power",
            value=FOO,  # type: ignore[arg-type]
        )

    # Setting scalar magnitude as value for a units-aware parameter should raise an error
    with pytest.raises(errors.ParameterValueError):
        _power_no_quantity_mag: Parameter[float, units_types.Power] = Parameter(
            data_type=float,
            quantity_type=quantity_types.Power,
            description="Units-aware parameter for thermal power",
            value=SCALAR_MAGNITUDE,  # type: ignore[arg-type]
        )

    # Setting a PintQuantity as value for a units-aware parameter should raise an error
    with pytest.raises(errors.ParameterValueError):
        _power_no_quantity_pint: Parameter[float, units_types.Power] = Parameter(
            data_type=float,
            quantity_type=quantity_types.Power,
            description="Units-aware parameter for thermal power",
            value=PintQuantity(SCALAR_MAGNITUDE, "kW"),  # type: ignore[arg-type]
        )

    # Setting a Quantity as value for a units-aware parameter should
    # be reflected in its quantity and pint quantity properties
    power_quantity: Parameter[float, units_types.Power] = Parameter(
        data_type=float,
        quantity_type=quantity_types.Power,
        description="Units-aware parameter for thermal power",
        value=Quantity(
            quantity_type=quantity_types.Power,
            magnitude=SCALAR_MAGNITUDE,
            units=units_collections.Power.kW,
        ),
    )
    pint.testing.assert_equal(
        power_quantity.pint_quantity, PintQuantity(SCALAR_MAGNITUDE, "kW")
    )
    assert power_quantity.quantity == Quantity(
        quantity_type=quantity_types.Power,
        magnitude=SCALAR_MAGNITUDE,
        units=units_collections.Power.kW,
    )

    # Setting a magnitude/units as value for a units-aware parameter should
    # be reflected in its quantity and pint quantity properties
    power_mag_units: Parameter[float, units_types.Power] = Parameter(
        data_type=float,
        quantity_type=quantity_types.Power,
        description="Units-aware parameter for thermal power",
        magnitude=SCALAR_MAGNITUDE,
        units=units_collections.Power.kW,
    )
    pint.testing.assert_equal(
        power_mag_units.pint_quantity, PintQuantity(SCALAR_MAGNITUDE, "kW")
    )
    assert power_mag_units.quantity == Quantity(
        quantity_type=quantity_types.Power,
        magnitude=SCALAR_MAGNITUDE,
        units=units_collections.Power.kW,
    )

    # Setting a magnitude w/o units as value or a units-aware parameter
    # should raise a warning
    with pytest.warns():
        _power_mag_no_units: Parameter[float, units_types.Power] = Parameter(
            data_type=float,
            quantity_type=quantity_types.Power,
            description="Units-aware parameter for thermal power",
            magnitude=SCALAR_MAGNITUDE,
        )
    with pytest.warns():
        _power_units_no_mag: Parameter[float, units_types.Power] = Parameter(
            data_type=float,
            quantity_type=quantity_types.Power,
            description="Units-aware parameter for thermal power",
            units=units_collections.Power.kW,
        )

    # Setting an incompatible data type for a units-naive parameter should raise an error
    with pytest.raises(errors.ParameterValueError):
        _param_wrong_data_type: Parameter[float, units_types.NoUnits] = (
            Parameter(
                data_type=float,
                description="Units-naive parameter",
                value=FOO,  # type: ignore[arg-type]
            )
        )
