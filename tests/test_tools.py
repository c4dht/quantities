import numpy as np
import pandas as pd
import pint.errors
import pint_pandas
import pytest

from src.qnit import (
    Parameter,
    PintQuantity,
    quantity_types,
    tools,
    units_collections,
    units_types,
)


def test_to():
    """
    Test the tools.to() function
    This function is currently trivial and has no test cases
    """
    pass


def test_interpolation():
    """
    Test the tools.InterpolationQuantity() functionality
    """

    interpolation_temperature: Parameter[float, units_types.Temperature] = (
        Parameter(
            data_type=float,
            quantity_type=quantity_types.Temperature,
            description="Temperature values for interpolation",
            magnitude=np.array(
                [
                    30.000,
                    32.000,
                    34.000,
                    36.000,
                    38.000,
                    40.000,
                    42.000,
                    44.000,
                    46.000,
                    48.000,
                    50.000,
                ]
            ),
            units=units_collections.Temperature.deg_C,
            value_comment="[Kind2013, pp. 176-195]",
        )
    )

    interpolation_water_density: Parameter[float, units_types.Density] = (
        Parameter(
            data_type=float,
            quantity_type=quantity_types.Density,
            description="Corresponding water density values for interpolation",
            magnitude=np.array(
                [
                    995.65,
                    995.03,
                    994.38,
                    993.69,
                    992.97,
                    992.22,
                    991.44,
                    990.64,
                    989.80,
                    988.94,
                    988.05,
                ]
            ),
            units=units_collections.Density.kg_per_m3,
            value_comment="[Kind2013, pp. 176-195]",
        )
    )

    temperature: Parameter[float, units_types.Temperature] = Parameter(
        data_type=float,
        quantity_type=quantity_types.Temperature,
        description="Temperature argument value",
        magnitude=45,
        units=units_collections.Temperature.deg_C,
    )

    pressure: Parameter[float, units_types.Pressure] = Parameter(
        data_type=float,
        quantity_type=quantity_types.Pressure,
        description="Pressure argument value",
        magnitude=45,
        units=units_collections.Pressure.mbar,
    )

    # Implementation of tools.InterpolationQuantity() for pint quantities series
    water_density = tools.InterpolationQuantity(
        x=pd.Series(
            data=pint_pandas.PintArray(
                values=interpolation_temperature.pint_quantity
            )
        ),
        y=pd.Series(
            data=pint_pandas.PintArray(
                values=interpolation_water_density.pint_quantity
            )
        ),
        kind="linear",
        bounds_error=True,
    )

    # By entering an argument pint quantity in [] the resulted interpolation
    # pint quantity is given back
    assert water_density[temperature.pint_quantity] == PintQuantity(
        990.22, "kg/m³"
    )

    # By entering an argument pint quantity in [] different from quantity
    # defined for x should raise pint's DimensionalityError
    with pytest.raises(pint.errors.DimensionalityError):
        assert water_density[pressure.pint_quantity] == PintQuantity(
            990.22, "kg/m³"
        )
