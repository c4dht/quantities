"""
Tests for the Quantity class
"""

from typing import Tuple

import numpy as np
import pytest

from src.qnit import (
    units_collections,
    errors,
    PintQuantity,
    Quantity,
)
from src.qnit.data_types import Magnitude
from src.qnit.quantity_types import QuantityType, units_types
from tests import TEST, SCALAR_MAGNITUDE, ARRAY_MAGNITUDE
from tests.conftest import (
    thermal_power,
    thermal_power_set,
    thermal_power_array_set,
)

THERMAL_POWER_SET_FIXTURES = (
    thermal_power_set.__name__,
    thermal_power_array_set.__name__,
)


@pytest.mark.parametrize("thermal_power_set_func", THERMAL_POWER_SET_FIXTURES)
def test_quantity(thermal_power_set_func: str, request: pytest.FixtureRequest):
    """
    Test Quantity.quantity
    :param thermal_power_set_func: Name of the thermal power fixture function
    """
    fixture_return: Tuple[
        Quantity[units_types.Power],
        Magnitude,
    ] = request.getfixturevalue(thermal_power_set_func)
    thermal_power_set, magnitude = fixture_return

    assert thermal_power_set.pint_quantity is not None

    # Quantity in given units
    assert np.equal(thermal_power_set.pint_quantity.m_as("kW"), magnitude).all()

    # Quantity in specific units (this is upstream Pint functionality)
    assert np.equal(
        thermal_power_set.pint_quantity.to("MW").magnitude,
        np.divide(magnitude, 1e3),
    ).all()
    assert np.equal(
        thermal_power_set.pint_quantity.to(units_collections.Power.kW).magnitude,
        magnitude,
    ).all()

    # Setting non-float values should raise a QuantityValueError
    with pytest.raises(errors.QuantityValueError):
        thermal_power_set.pint_quantity = PintQuantity(TEST, "MW")
    with pytest.raises(errors.QuantityValueError):
        thermal_power_set.pint_quantity = PintQuantity([TEST, TEST], "MW")

    # Setting incompatible units should raise a QuantityUnitsError
    with pytest.raises(errors.QuantityUnitsError):
        thermal_power_set.pint_quantity = PintQuantity(magnitude, "MWh")


@pytest.mark.parametrize("thermal_power_set_func", THERMAL_POWER_SET_FIXTURES)
def test_magnitude(thermal_power_set_func: str, request: pytest.FixtureRequest):
    """
    Test Quantity.magnitude
    :param thermal_power_set_func: Name of the thermal power fixture function
    """
    fixture_return: Tuple[
        Quantity[units_types.Power],
        Magnitude,
    ] = request.getfixturevalue(thermal_power_set_func)
    thermal_power_set, magnitude = fixture_return

    # Magnitude in internal units
    internal_magnitude = thermal_power_set.magnitude(
        units=units_collections.Power.kW
    )
    assert np.equal(internal_magnitude, magnitude).all()

    # Magnitude in specific units
    specific_magnitude = thermal_power_set.magnitude(
        units=units_collections.Power.MW
    )
    assert np.equal(
        specific_magnitude,
        np.divide(magnitude, 1e3),
    ).all()

    # Giving units as string should work, although type checkers will complain
    # noinspection PyTypeChecker
    specific_magnitude = thermal_power_set.magnitude(units="kW")  # type: ignore[arg-type]
    assert np.equal(specific_magnitude, magnitude).all()

    # Non-compatible units should raise a QuantityUnitsError
    with pytest.raises(errors.QuantityUnitsError):
        # noinspection PyTypeChecker
        thermal_power_set.magnitude(units=units_collections.Energy.MWh)  # type: ignore[arg-type]


"""
Power quantity type without defined default display units
"""
PowerNoDDU = QuantityType(
    units_type=units_types.Power,
    internal_units=units_collections.Power.W,
    available_units=units_collections.Power(),
)


@pytest.mark.parametrize("thermal_power_set_func", THERMAL_POWER_SET_FIXTURES)
def test_display_magnitude(
    thermal_power_set_func: str, request: pytest.FixtureRequest
):
    """
    Test Quantity.display_magnitude
    :param thermal_power_set_func: Name of the thermal power fixture function
    """
    fixture_return: Tuple[
        Quantity[units_types.Power],
        Magnitude,
    ] = request.getfixturevalue(thermal_power_set_func)
    thermal_power_set, magnitude = fixture_return

    assert np.equal(thermal_power_set.display_magnitude, magnitude).all()

    # Quantities with no defined default display units
    # should return magnitudes as internal units
    thermal_power_no_ddu: Quantity[units_types.Power] = Quantity(
        quantity_type=PowerNoDDU,
        description="Test quantity for thermal power in kW",
        magnitude=SCALAR_MAGNITUDE,
        units=units_collections.Power.kW,
    )
    assert thermal_power_no_ddu.display_magnitude == SCALAR_MAGNITUDE * 1e3
    thermal_power_no_ddu_v: Quantity[units_types.Power] = Quantity(
        quantity_type=PowerNoDDU,
        description="Test quantity for thermal power in kW",
        magnitude=ARRAY_MAGNITUDE,
        units=units_collections.Power.kW,
    )
    assert np.equal(
        thermal_power_no_ddu_v.display_magnitude,
        np.multiply(ARRAY_MAGNITUDE, 1e3),
    ).all()


@pytest.mark.parametrize("thermal_power_set_func", THERMAL_POWER_SET_FIXTURES)
def test_internal_magnitude(
    thermal_power_set_func: str, request: pytest.FixtureRequest
):
    """
    Test Quantity.internal_magnitude
    :param thermal_power_set_func: Name of the thermal power fixture function
    """
    fixture_return: Tuple[
        Quantity[units_types.Power],
        Magnitude,
    ] = request.getfixturevalue(thermal_power_set_func)
    thermal_power_set, magnitude = fixture_return

    assert np.equal(
        thermal_power_set.internal_magnitude, np.multiply(magnitude, 1e3)
    ).all()


def test_set(thermal_power: Quantity[units_types.Power]):
    """
    Test Quantity.set
    :param thermal_power: [Fixture] An unset thermal power quantity
    """

    # Setting scalar and array magnitudes should just work
    thermal_power.set(
        magnitude=SCALAR_MAGNITUDE, units=units_collections.Power.kW
    )
    thermal_power.set(
        magnitude=ARRAY_MAGNITUDE, units=units_collections.Power.kW
    )

    # Setting a non-float value should raise a QuantityValueError
    with pytest.raises(errors.QuantityValueError):
        # noinspection PyTypeChecker
        thermal_power.set(
            magnitude="schwifty-three",  # type: ignore[arg-type]
            units=units_collections.Power.kW,
        )
    with pytest.raises(errors.QuantityValueError):
        # noinspection PyTypeChecker
        thermal_power.set(
            magnitude=("schwifty-three", "schwifty-four"),  # type: ignore[arg-type]
            units=units_collections.Power.kW,
        )
    with pytest.raises(errors.QuantityValueError):
        # noinspection PyTypeChecker
        thermal_power.set(
            magnitude=False,  # type: ignore[arg-type]
            units=units_collections.Power.kW,
        )
    with pytest.raises(errors.QuantityValueError):
        # noinspection PyTypeChecker
        thermal_power.set(
            magnitude=None,  # type: ignore[arg-type]
            units=units_collections.Power.kW,
        )
    with pytest.raises(errors.QuantityValueError):
        # noinspection PyTypeChecker
        thermal_power.set(
            magnitude={"a": 12},  # type: ignore[arg-type]
            units=units_collections.Power.kW,
        )

    # Setting non-compatible units should raise a QuantityUnitsError
    with pytest.raises(errors.QuantityUnitsError):
        # noinspection PyTypeChecker
        thermal_power.set(
            magnitude=SCALAR_MAGNITUDE,
            units=units_collections.Energy.kWh,  # type: ignore[arg-type]
        )
