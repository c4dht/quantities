import numpy as np
import numpy.typing as npt

FOO = "Foo"
BAR = "Bar"
BAZ = "Baz"
QUX = "Qux"
TEST = "Test"
SCALAR_MAGNITUDE = 53
ARRAY_MAGNITUDE: npt.NDArray[np.number] = np.array([53, 54, 55])
