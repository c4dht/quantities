from typing import Iterator

import numpy as np
import numpy.typing as npt
import pytest


from src.qnit import (
    Quantity,
    Parameter,
    quantity_types,
    units_collections,
    units_types,
)
from tests import SCALAR_MAGNITUDE, ARRAY_MAGNITUDE, FOO, BAR


# Type aliases for Quantity
QuantityFixtReturn = Iterator[Quantity[units_types.Power]]
QuantitySetFixtReturn = Iterator[tuple[Quantity[units_types.Power], int]]
QuantitySetArrayFixtReturn = Iterator[
    tuple[Quantity[units_types.Power], npt.NDArray[np.number]]
]
# Type aliases for units-aware Parameter
ParameterUnitsAwareFixtReturn = Iterator[Parameter[float, units_types.Power]]
ParameterUnitsAwareSetFixtReturn = Iterator[
    tuple[Parameter[float, units_types.Power], float]
]
ParameterUnitsAwareSetArrayFixtReturn = Iterator[
    tuple[Parameter[float, units_types.Power], npt.NDArray[np.number]]
]
# Type aliases for units-naive Parameter
ParameterUnitsNaiveFixtReturn = Iterator[Parameter[str, units_types.NoUnits]]
ParameterUnitsNaiveSetFixtReturn = Iterator[
    tuple[Parameter[str, units_types.NoUnits], str]
]
ParameterUnitsNaiveSetArrayFixtReturn = Iterator[
    tuple[Parameter[str, units_types.NoUnits], npt.NDArray[np.str_]]
]


@pytest.fixture
def thermal_power() -> QuantityFixtReturn:
    """
    An unset thermal power quantity.
    This fixture's purpose is preventing duplicate code
    in the test functions' setup.
    """
    thermal_power: Quantity[units_types.Power] = Quantity(
        quantity_type=quantity_types.Power,
        description="Test quantity for thermal power",
    )
    yield thermal_power


@pytest.fixture
def thermal_power_set(
    thermal_power: Quantity[units_types.Power],
) -> QuantitySetFixtReturn:
    """
    A thermal power set with a scalar quantity.
    This fixture's purpose is preventing duplicate code
    in the test functions' setup.
    """
    thermal_power.set(
        magnitude=SCALAR_MAGNITUDE,
        units=units_collections.Power.kW,
    )
    yield thermal_power, SCALAR_MAGNITUDE


@pytest.fixture
def thermal_power_array_set(
    thermal_power: Quantity[units_types.Power],
) -> QuantitySetArrayFixtReturn:
    """
    A thermal power quantity set with an array magnitude.
    This fixture's purpose is preventing duplicate code
    in the test functions' setup.
    """
    thermal_power.set(
        magnitude=ARRAY_MAGNITUDE,
        units=units_collections.Power.kW,
    )
    yield thermal_power, ARRAY_MAGNITUDE


@pytest.fixture
def thermal_power_parameter() -> ParameterUnitsAwareFixtReturn:
    """
    An unset thermal power parameter.
    This fixture's purpose is preventing duplicate code
    in the test functions' setup.
    """
    thermal_power_parameter: Parameter[float, units_types.Power] = Parameter(
        data_type=float,
        quantity_type=quantity_types.Power,
        description="Units-aware test parameter for thermal power",
        value_comment="Random value developers come up with",
    )
    yield thermal_power_parameter


@pytest.fixture
def thermal_power_parameter_set(
    thermal_power_parameter: Parameter[float, units_types.Power],
) -> ParameterUnitsAwareSetFixtReturn:
    """
    A thermal power parameter set with a scalar magnitude.
    This fixture's purpose is preventing duplicate code
    in the test functions' setup.
    """
    thermal_power_parameter.set_magnitude_units(
        magnitude=SCALAR_MAGNITUDE, units=units_collections.Power.kW
    )
    yield thermal_power_parameter, SCALAR_MAGNITUDE


@pytest.fixture
def thermal_power_parameter_array_set(
    thermal_power_parameter: Parameter[float, units_types.Power],
) -> ParameterUnitsAwareSetArrayFixtReturn:
    """
    A thermal power parameter set with an array magnitude.
    This fixture's purpose is preventing duplicate code
    in the test functions' setup.
    """
    thermal_power_parameter.set_magnitude_units(
        magnitude=ARRAY_MAGNITUDE, units=units_collections.Power.kW
    )
    yield thermal_power_parameter, ARRAY_MAGNITUDE


@pytest.fixture
def fuel_type_parameter() -> ParameterUnitsNaiveFixtReturn:
    """
    An unset fuel type parameter.
    This fixture's purpose is preventing duplicate code
    in the test functions' setup.
    """
    fuel_type_parameter: Parameter[str, units_types.NoUnits] = Parameter(
        data_type=str, description="Units-naive test parameter for fuel type"
    )
    yield fuel_type_parameter


@pytest.fixture
def fuel_type_parameter_set(
    fuel_type_parameter: Parameter[str, units_types.NoUnits],
) -> ParameterUnitsNaiveSetFixtReturn:
    """
    A fuel type parameter set with units-naive value.
    This fixture's purpose is preventing duplicate code
    in the test functions' setup.
    """
    fuel_type_parameter.value = FOO
    yield fuel_type_parameter, FOO


@pytest.fixture
def fuel_type_parameter_array_set(
    fuel_type_parameter: Parameter[str, units_types.NoUnits],
) -> ParameterUnitsNaiveSetArrayFixtReturn:
    """
    A fuel type parameter set with an array of units-naive values.
    This fixture's purpose is preventing duplicate code
    in the test functions' setup.
    """
    fuel_type_parameter.value = np.array([FOO, BAR])
    yield fuel_type_parameter, np.array([FOO, BAR])
