## 0.5.0

### Major Review

* Restructured the units and quantity types library by categories of physical domain and introduced inheritance
* Modules and module members renamed to match `pint`s terminology concerning "units" vs. "unit"
* The `pint_tools` module is now called `tools`
* Added `magnitude` and `units` parameters to `Parameter`'s initializer
* Added `Parameter.set_value` to be able to set a parameter value comment together with a value
* Instances of `Quantity` can now be compared for equality
* Deactivated `pint`'s experimental caching feature
* Several typing improvements
* Removed redundant and erroneous tool functionality
* Expanded test coverage to cover all module members

### Fixes

* Parameter.value setter did not handle sequence-like arguments correctly

## 0.4.0

The package has been renamed!

**qnit** [kjuː.nɪt]

### Changes

- Semantics of a "parameter value" is now:
  - A `Quantity` instance for units-aware parameters
  - A scalar or array value of type `ParamDataT` for units-naive parameters
- Preset parameters for `Quantity` ans `Parameter` initialization have changed to more reasonable semantics. It's now `Quantity(magnitude=..., units=...)` and `Parameter(value=...)`
- `Parameter` has now distinct properties for getting its `Quantity` and its `pint.Quantity`, i.e. `Parameter.quantity` and `Parameter.pint_quantity`, respectively. Analogously and to be consistent, `Quantity.quantity` has changed to `Quantity.pint_quantity`.
- Integrated `VectorQuantity` with `Quantity` and `VectorParameter` with `Parameter` by using `pint`'s built-in array support
- Added `Quantity.magnitude_is_scalar`

### Fixes

- Improved typing support
- Raising a warning instead of an error when accessing an unset quantity lead to unexpected behaviour
- Accessing the quantity of a units-naive parameter did not raise a specific value error
- Parameter magnitudes were not typed so that they could be arrays
- A Parameter's description was unintendedly propagated as the underlying quantity description
- The parameter data type var did not permit non-numpy types

## 0.3.0

### Changes

- mypy compliance and typing support
- Added the writable `Parameter.value` property for units-naive parameter instances
- Restricted access to Parameter magnitudes to units-aware instances
- Removed generic `Parameter.set` for values
- Added `Parameter.set_magnitude_units` which is accesible for units-aware parameter instances
- Removed the `quantity` parameter from `Quantity.set`. Users can assign `Quantity.quantity` directly instead.
- Removed the `Quantity.valueComment` property and all associated method parameters

### Fixes

- Using `Quantity.set` did not require specifying both magnitude AND units
- Setting a defaults for `Quantity` and `Parameter` did not require specifying both magnitude AND units

### Development

- Added `tox` for automated tests and typing-tests
